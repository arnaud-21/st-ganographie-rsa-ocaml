open Num;;
open Big_int;;
open Printf;;



(* test si arguments *)
let test_arg =  if (Array.length Sys.argv) = 3 then
    [|Sys.argv.(1);Sys.argv.(2)|]
    else 
    [|"400";"432"|];;

let rec nbr_digit n = 
  if n = 0 then "1"
  else 
    nbr_digit (n - 1) ^ (string_of_int (Random.int 9));;



(* hauteur_img et largeur_img doivent etre distinct*)
let hauteur_img = test_arg.(0);;
let largeur_img = test_arg.(1);;

print_string "Les arguments sont ";;
print_string ( ( hauteur_img ^ " et " ^ largeur_img ^ "\n" ) );;

print_string ("Clefs generees pour une image de taille " ^ hauteur_img ^ "x" ^ largeur_img ^ "\n" );;

let taillel = let h_img = int_of_string hauteur_img in (nbr_digit h_img);;
let tailleh = let l_img = int_of_string largeur_img in (nbr_digit l_img);;

let ecrire message fichier =
  (* Ecrire message vers fichier *)
  let oc = open_out fichier in    
  fprintf oc "%s\n" message;
  close_out oc;;  

(* DEBUT Auteur : Danny Sleator *)

let ( mod ) a b = mod_num a b
let ( // ) a b = quo_num a b
let n0 = num_of_int 0
let n1 = num_of_int 1
let n2 = num_of_int 2
let n3 = num_of_int 3
let n5 = num_of_int 5
let n7 = num_of_int 7
let n10 = num_of_int 10

let rec powermod a p n = 
  if p =/ n0 then n1 else
    let sq x = (square_num x) mod n in
    let x = sq (powermod a (p // n2) n) in
      if p mod n2 =/ n0 then x else (a */ x) mod n

let _ = Random.init 100

(* This function returns the size of the number in bits.  So if n=1023 then
   size n is 9.  If it's 1024 then the size is 10.  works for n>0. *)

let rec size n = if n <=/ n1 then 1 else 1 + (size (n // n2))

(* generates a random number with s bits *)
let rec random_num s = 
  if (s <= 30) then num_of_int ((Random.bits ()) land ((1 lsl s)-1)) else
    let r = random_num (s-30) in
      (r */ (num_of_int (1 lsl 30))) +/ (num_of_int (Random.bits()))
  
(* This function selects a random number in the range [2...n-1]  *)
let rec select_random n =
  let s = size n in
  let r = random_num s in
    if (r </ n & r >/ n1) then r else  select_random n;;

let miller_rabin n =
  if n <=/ n10 then (n=/n2 or n=/n3 or n=/n5 or n=/n7) else
    if (n mod n2 =/ n0 or n mod n3 =/ n0 or n mod n5 =/ n0 or n mod n7 =/ n0) then false else
      let rec remove_twos m = 
	let h = m // n2 in
	  if (h +/ h </ m) then (0,m) else
	    let (s,d) = remove_twos h in (s+1,d)
      in
      let (s,d) = remove_twos (n -/ n1) in   (* so 2^s = n-1 *)
      let is_witness_to_compositeness a =
	let x = powermod a d n in
	  if x =/ n1 or x =/ (n -/ n1) then false else
	    let rec loop x r =    	    (* at this point x = a^(d * 2^r) mod n *)
	      if x =/ n1 or r=s then true else 
		if x =/ (n -/ n1) then false else 
		  loop ((x */ x) mod n) (r+1)
	    in loop ((x */ x) mod n) 1
      in
      let rec try_witnesses k = 
	if k=0 then true else
	  if (is_witness_to_compositeness (select_random n)) then ( (*Printf.printf "temoin essaye %d fois \n" (31-k);*) false)
	  else try_witnesses (k-1)
      in
	try_witnesses 30

let rec next_prime_up n = 
  let nn = n +/ n1 in
  if miller_rabin nn then nn else next_prime_up nn;;


(* FIN Auteur : Danny Sleator *)

let p = string_of_num (next_prime_up (num_of_string taillel));;

let q = string_of_num (next_prime_up (num_of_string tailleh));;

let n = mult_big_int (big_int_of_string p) (big_int_of_string q);;

let phi_n = mult_big_int (add_big_int (big_int_of_string "-1") (big_int_of_string p)) (add_big_int (big_int_of_string "-1") (big_int_of_string q));;

let rec gen_e r = if r > 3 then r 
  else
    gen_e (Random.int 100);;

let e_par_def = gen_e (Random.int 100);;

let e = big_int_of_string (string_of_int e_par_def);;

(* vérifexpo tel que  ( pgcd e (phi_n) = 1 ) *)
let verifexpo e =
    if  eq_big_int (big_int_of_string "1") ( gcd_big_int e (phi_n) )   then 1
    else 0;;

(* change clef publique e tant que (verifexpo e)!=1 *)
let rec verif_boucle e = 
  if (verifexpo e) = 1 then e
  else verif_boucle ( big_int_of_string (string_of_num (next_prime_up (num_of_string (string_of_big_int (add_big_int unit_big_int e ) ) ))) );;

let e = (verif_boucle e);; 

string_of_int (verifexpo e);; (* affiche 1 si reussi*)
string_of_big_int e;; (* affiche e *)
string_of_big_int n;; (* affiche n*)

(* Ecriture des clefs publiques e et n dans un fichier chacun*)
ecrire (string_of_big_int n) "n.dat";;
ecrire (string_of_big_int e) "e.dat";;

(* Generation clef privée *)

(* fonction pgcd_etendu*)
let rec pgcd_etendu a b = 
    let rec eucli r u v r' u' v' =
        if ( (eq_big_int r' (big_int_of_string "0")) = false ) then
            eucli r' u' v' (add_big_int r (minus_big_int (mult_big_int (div_big_int r r') r')) ) (add_big_int u (minus_big_int (mult_big_int (div_big_int r r') u')) ) (add_big_int v (minus_big_int (mult_big_int (div_big_int r r') v')) )
            (* eucli r' u' v' (r - (r/r') * r') (u - (r/r') * u') (v - (r/r') * v') *)
        else [r;u;v]
    in eucli a (big_int_of_string "1") (big_int_of_string "0") b (big_int_of_string "0") (big_int_of_string "1");;


(* fonction si pgcd negatif *)
let rec cherche_d k t= 
    if lt_big_int (add_big_int t (mult_big_int k (phi_n) ) ) (big_int_of_string "2") then cherche_d (add_big_int k (big_int_of_string "1")) t
    else
        (add_big_int t (mult_big_int k (phi_n) ) );;


let d = let t = (List.nth ( pgcd_etendu e (phi_n) ) 1) in 
    if gt_big_int t (big_int_of_string "2") then t
    else cherche_d (big_int_of_string "1") t;;

string_of_big_int d;; (* affiche d*)
ecrire (string_of_big_int d) "d.dat";;

print_string "La clef n se situe dans n.dat \n";;

print_string "La clef e se situe dans e.dat \n";;

print_string "La clef d se situe dans d.dat \n";;



