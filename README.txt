./genere tp tq
tp et tq sont les nombres de digits à générer aléatoirement pour les clefs p et q. p et q sont des premiers constituant la clef publique n.
Des fichiers n.dat, e.dat et d.dat seront générés à la fin de l'exécution.

./chiffrer n.dat e.dat Unmessage ub.jpg
La commande prend pour arguments : le chemin des clefs publiques n et e, un message et une image au format PNG/JPG.
Une image contenant le message dissimulé : chiffre.png est obtenue à la fin de l'exécution.

./dechiffrer n.dat d.dat chiffre.png 801
La commande prend pour arguments : le chemin des clefs  n et d, une image et la taille du message chiffré (obtenu precedemment dans l'affichage de ./chiffrer)


Pour lancer les programmes avec ocaml : rlwrap ocaml graphics.cma images.cmo nums.cma

