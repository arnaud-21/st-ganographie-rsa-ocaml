open Graphics;;
open Images;;
open Big_int;;
Graphics.open_graph(" 400x400");;
Graphics.set_window_title "Image de base";;
let im = lire_image "ub.jpg" ;;
let hauteur = 400;;
let largeur = 400;; 

print_string "\n Programme pour chiffrer un message dans une image : \n";;
(* Fonction pour obtenir les matrices r, g et b d'une image *)
let decompose_rgb=
    let r = Array.make_matrix hauteur largeur 0 
    and g = Array.make_matrix hauteur largeur 0 
    and b = Array.make_matrix hauteur largeur 0 in
        for i = 0 to hauteur - 1 do
            for j = 0 to largeur - 1 do 
                r.(i).(j) <- im.(i).(j)/(256*256);
                g.(i).(j) <- ( im.(i).(j) - r.(i).(j) * 256 * 256 ) /256;
                b.(i).(j) <- im.(i).(j) - r.(i).(j) * 256 * 256 - g.(i).(j) * 256;
            done
        done;
        [|r;g;b|] ;;

(* fonction pgcd_etendu*)
let rec pgcd_etendu a b = 
    let rec eucli r u v r' u' v' =
        if ( (eq_big_int r' (big_int_of_string "0")) = false ) then
            eucli r' u' v' (add_big_int r (minus_big_int (mult_big_int (div_big_int r r') r')) ) (add_big_int u (minus_big_int (mult_big_int (div_big_int r r') u')) ) (add_big_int v (minus_big_int (mult_big_int (div_big_int r r') v')) )
            (* eucli r' u' v' (r - (r/r') * r') (u - (r/r') * u') (v - (r/r') * v') *)
        else [r;u;v]
    in eucli a (big_int_of_string "1") (big_int_of_string "0") b (big_int_of_string "0") (big_int_of_string "1");;


(* Methode de lecture de la 1ere ligne ds d'un fichier *)
let lire fichier = 
let ic = open_in fichier in
    let line = input_line ic in
        close_in ic;
    line;;

(* 1ere etape recup de n et e les clefs publiques *)

(* recup de p, q *)
(* cle publique constitue de n et e*)
(* cle privee = d *)

(* test si arguments *)
let test_arg2 =  if (Array.length Sys.argv) = 5 then
    [|Sys.argv.(1);Sys.argv.(2);Sys.argv.(3);Sys.argv.(4)|]
    else 
    [|"n.dat";"e.dat";"Unmessagecache";"ub.jpg"|];;


let n = big_int_of_string (lire test_arg2.(0));;
string_of_big_int n;;

let e = big_int_of_string (lire test_arg2.(1));;
string_of_big_int e;;

let im = lire_image test_arg2.(3) ;;

print_string ( "Clef n stockee dans " ^ (test_arg2.(0)) ^ "\n" );;
print_string ( "Clef e stockee dans " ^ (test_arg2.(1)) ^ "\n" );;
print_string ("Image : " ^ test_arg2.(3) ^ "\n" );;





let max x y = if gt_big_int x y then x else y;;

let rec maxliste l = 
    match l with
    | [] -> unit_big_int
    | t::q -> max t (maxliste q);;


(* PARTIE CHIFFREMENT DU MSG*)
(* RECUPERER DERNIER BIT ET METTRE DANS UN TABLEAU *)


(* string vers liste de char *)
let rec string_to_list c = match c with
    | "" -> []
    | c -> (String.get c 0 ) :: (string_to_list (String.sub c 1 ((String.length c)-1)) );;

let string_to_char c = Char.chr (int_of_string c);; 


let rec list_2_par_2 l = match l with
    | [] -> []
    | hd::x::tl -> (((Char.escaped hd) ^ (Char.escaped x) ))::(list_2_par_2 tl)
    | hd::tl ->   (Char.escaped hd)::list_2_par_2 tl;;



let bin_of_int de =
  if de < 0 then invalid_arg "bin_of_int" else
  if de = 0 then "0" else
  let rec aux acc de =
    if de = 0 then acc else
    aux (string_of_int (de land 1) :: acc) (de lsr 1)
  in
  String.concat "" (aux [] de);;

(* pas utile *)
let rec char_to_octet c = 
    if String.length ( bin_of_int (Char.code c) ) < 8 then
        String.concat "" ["0";( bin_of_int (Char.code c))] 
    else "Erreur > 8";;

let rec add_0 s = 
    if (String.length s) < 8 then
        add_0 (String.concat "" ["0"; s])
    else
        s;;

(* int vers string *)
let int_to_octet s = add_0 (bin_of_int s);;


let rec string_to_octetl l = match l with
    | [] -> []
    | hd::tl -> ( int_to_octet (int_of_string hd))::(string_to_octetl tl);;


(* fonction pour transformer une liste de charactere en liste de int  *)

(* fonction appliquee sur liste de char, retoune liste de string *)
(* Les elements de la liste char -> int -> string *)
let rec charl_to_intl l = match l with
    | [] -> []
    | hd::tl -> (string_of_int (Char.code hd) )::(charl_to_intl tl);;



let rec list_to_string l = match l with
    | [] -> ""
    | hd::tl -> hd ^ (list_to_string tl);;


(* pour chaque octet on crypte la valeur *)

let convert ll_to_aa = Array.of_list (List.map Array.of_list ll_to_aa);;


(* les listes sont immutables donc on utilise String.sub et l'operateur de concatenation*)
let eff_last_char s =
  if s = "" then "" else
  String.sub s 0 ((String.length s) - 1) ^ "0";;

let eff_last_bit n = 
   int_of_string ("0b" ^ eff_last_char ( bin_of_int n ) );;


let rec expo_mod x k n =
    if eq_big_int k (big_int_of_string "0") then big_int_of_string "1"
	else
		let puiss = expo_mod x (div_big_int k (big_int_of_string "2") ) n in 
    if eq_big_int ( mod_big_int k (big_int_of_string "2") ) (big_int_of_string "0")
			then (mod_big_int (square_big_int puiss) n)
			else mod_big_int ( mult_big_int (mod_big_int (square_big_int puiss) n) x ) n;;


(*
let capa = if (hauteur * largeur) mod 4 = 0 then
                (hauteur * largeur)/8
           else
               (hauteur * largeur)/8 + (hauteur*largeur) mod 4;; 


let bruit_m = 
        let longueur = String.length test_arg2.(2) in 
            if capa mod 8 = 0 then
                capa - ( longueur * 8 )
            else
                capa - ( longueur * 8 ) + 1;; (* et dernier caractere illisible car codé sur 1 pixel *)

let rec gen_bruit b =
    if b = 0 then
        "" 
    else
        (String.make 1 (Char.chr ( (Random.int (126-33)) + 33) ) ) ^ gen_bruit (b-1);;
*)

let m_avg = charl_to_intl (List.map Char.uppercase (string_to_list ( test_arg2.(2) ) ) );;


let m = big_int_of_string ( list_to_string (m_avg) );; (* m est le message*)
print_string ( "Le message est : " ^ test_arg2.(2) ^ "\n" );;


let mchiffre =  expo_mod m e n;;
print_string ("Le message chiffre est : " ^  (string_of_big_int mchiffre) ^ "\n");;
print_string ("TAILLE du message chiffre = " ^ (string_of_int(String.length (string_of_big_int mchiffre))) ^ "\n" );;

let m_liste = string_to_list (string_of_big_int mchiffre);;

let m_bit = string_to_list (list_to_string ( string_to_octetl (list_2_par_2 ( m_liste) ) ) ) ;;




(* modulo ? On test deja de mettre le message cache dans la matrice R *)
let msg_to_array =
    let table = Array.make_matrix hauteur largeur 0 in
        for i = 0 to hauteur - 1 do
            for j = 0 to largeur - 1 do
                if List.length m_bit > (largeur*i + j) then
                    (* effacage du bit de poids faible du pixel R(i,j) puis addition decimal avec le bit du msg crypté*)
                    table.(i).(j) <-  eff_last_bit decompose_rgb.(0).(i).(j) + int_of_string ( String.make 1 ( List.nth m_bit (largeur*i + j) ) )
                else 
                    table.(i).(j) <- decompose_rgb.(0).(i).(j)
            done
        done;
        table;;


let compose_rgb rgb= 
    let t = Array.make_matrix hauteur largeur (Graphics.rgb 255 0 0) in 
        for i = 0 to hauteur - 1 do
            for j = 0 to largeur - 1 do 
               t.(i).(j) <- Graphics.rgb rgb.(0).(i).(j) rgb.(1).(i).(j) rgb.(2).(i).(j)
            done
        done;
        t;;

(* fonction compose_rgb_c pour message chiffre dans composante r*)
let compose_rgb_c= 
    let t = Array.make_matrix hauteur largeur (Graphics.rgb 255 0 0) in 
        for i = 0 to hauteur - 1 do
            for j = 0 to largeur - 1 do 
               t.(i).(j) <- Graphics.rgb msg_to_array.(i).(j) decompose_rgb.(1).(i).(j) decompose_rgb.(2).(i).(j)
            done
        done;
        t;;

(* conversion du tab de pixels vers une image type graphics *)
let image_fenetre = Graphics.make_image im;; (* POUR AFFICHER l'image de base*)
(* dessin de l'image dans la fenetre ( a partir du coin inferieur gauche ) *)
Graphics.draw_image image_fenetre 0 0 ;;

(* Pour sauvegarder l'image chiffree*)
sauver_image (compose_rgb_c) "chiffre.png";;




let parite x = let s = (string_of_big_int x ) in big_int_of_string ( Char.escaped ( String.get s ( (String.length s) - 1 ) ) );; 

(* https://fr.wikipedia.org/wiki/Exponentiation_rapide *)
let rec expo_rapide x n = 
    if  eq_big_int n unit_big_int then x
    else if eq_big_int ( mod_big_int (parite n) (big_int_of_string "2") ) (big_int_of_string "0") then expo_rapide (mult_big_int x x) (div_big_int n (big_int_of_string "2") )
    else mult_big_int x ( expo_rapide (mult_big_int x x) ( div_big_int (add_big_int n (big_int_of_string "-1")) (big_int_of_string "2") )  );;





