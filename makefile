ok: genere chiffrer dechiffrer

genere: genere.ml
		ocamlc -o genere nums.cma genere.ml



chiffrer: chiffrer.ml 
		ocamlc -c images.mli
		ocamlc -c images.ml
	  	ocamlc -o chiffrer nums.cma graphics.cma images.cmo chiffrer.ml

dechiffrer: dechiffrer.ml
		ocamlc -o dechiffrer nums.cma graphics.cma images.cmo dechiffrer.ml

clean: 
	rm -fr genere chiffrer dechiffrer chiffre.png *.cmi *.o *.cmo *.cmx *.dat
