open Graphics;;
open Images;;
open Big_int;;
Graphics.open_graph(" 400x400");;
Graphics.set_window_title "Image chiffree";;
let im = lire_image "chiffre.png" ;;
let hauteur = 400;;
let largeur = 400;; 

print_string "\nProgramme pour dechiffrer le message dans l'image : \n";;

(* conversion du tab de pixels vers une image type graphics *)
let image_fenetre = Graphics.make_image im ;;
(* dessin de l'image dans la fenetre ( a partir du coin inferieur gauche ) *)
Graphics.draw_image image_fenetre 0 0 ;;


(* Fonction pour obtenir les matrices r, g et b d'une image *)
let decompose_rgb=
    let r = Array.make_matrix hauteur largeur 0 
    and g = Array.make_matrix hauteur largeur 0 
    and b = Array.make_matrix hauteur largeur 0 in
        for i = 0 to hauteur - 1 do
            for j = 0 to largeur - 1 do 
                r.(i).(j) <- im.(i).(j)/(256*256);
                g.(i).(j) <- ( im.(i).(j) - r.(i).(j) * 256 * 256 ) /256;
                b.(i).(j) <- im.(i).(j) - r.(i).(j) * 256 * 256 - g.(i).(j) * 256;
            done
        done;
        [|r;g;b|] ;;

(* fonction pgcd_etendu*)
let rec pgcd_etendu a b = 
    let rec eucli r u v r' u' v' =
        if ( (eq_big_int r' (big_int_of_string "0")) = false ) then
            eucli r' u' v' (add_big_int r (minus_big_int (mult_big_int (div_big_int r r') r')) ) (add_big_int u (minus_big_int (mult_big_int (div_big_int r r') u')) ) (add_big_int v (minus_big_int (mult_big_int (div_big_int r r') v')) )
            (* eucli r' u' v' (r - (r/r') * r') (u - (r/r') * u') (v - (r/r') * v') *)
        else [r;u;v]
    in eucli a (big_int_of_string "1") (big_int_of_string "0") b (big_int_of_string "0") (big_int_of_string "1");;



(* Methode de lecture de la 1ere ligne ds d'un fichier *)
let lire fichier = 
let ic = open_in fichier in
    let line = input_line ic in
        close_in ic;
    line;;


(* test si arguments *)
let test_arg =  if (Array.length Sys.argv) = 5 then
    [|Sys.argv.(1);Sys.argv.(2);Sys.argv.(3);Sys.argv.(4)|]
    else 
    [|"n.dat";"d.dat";"chiffre.png";(string_of_int(String.length (lire "n.dat") ))|];;

(* clef publique n*)
let n = big_int_of_string (lire test_arg.(0));;
print_string ("La clef n se situe dans " ^ test_arg.(0) ^ "\n");;

(* clef privee d *)
let d = big_int_of_string (lire test_arg.(1));;
print_string ("La clef d se situe dans " ^ test_arg.(1) ^ "\n");;

(* choix image *)
let im = lire_image test_arg.(2) ;;
print_string ("Image : " ^ test_arg.(2) ^ "\n");;

(* msg_chiff_taille *)
let msg_chiff_taille = int_of_string test_arg.(3);;
print_string ("Taille du msg chiffre : " ^ (string_of_int msg_chiff_taille) ^ "\n");;


(* string vers liste de char *)
let rec string_to_list c = match c with
    | "" -> []
    | c -> (String.get c 0 ) :: (string_to_list (String.sub c 1 ((String.length c)-1)) );;

let string_to_char c = Char.chr (int_of_string c);; 


let rec list_2_par_2 l = match l with
    | [] -> []
    | hd::x::tl -> (string_to_char ((Char.escaped hd) ^ (Char.escaped x) ))::(list_2_par_2 tl)
    | hd::tl ->  (string_to_char (Char.escaped hd))::list_2_par_2 tl;;


let bin_of_int de =
  if de < 0 then invalid_arg "bin_of_int" else
  if de = 0 then "0" else
  let rec aux acc de =
    if de = 0 then acc else
    aux (string_of_int (de land 1) :: acc) (de lsr 1)
  in
  String.concat "" (aux [] de);;

let rec char_to_octet c = 
    if String.length ( bin_of_int (Char.code c) ) < 8 then
        String.concat "" ["0";( bin_of_int (Char.code c))] 
    else "Erreur > 8";;

let rec charl_to_octetl l = match l with
    | [] -> []
    | hd::tl -> (char_to_octet hd)::(charl_to_octetl tl);;


(* fonction pour transformer une liste de charactere en liste de int  *)

(* fonction appliquee sur liste de char vers int vers string *)
let rec charl_to_intl l = match l with
    | [] -> []
    | hd::tl -> (string_of_int (Char.code hd) )::(charl_to_intl tl);;



let rec list_to_string l = match l with
    | [] -> ""
    | hd::tl -> hd ^ (list_to_string tl);;



(* (list list) to (array array) *)
let convert ll_to_aa = Array.of_list (List.map Array.of_list ll_to_aa);;




let rec expo_mod x k n =
    if eq_big_int k (big_int_of_string "0") then big_int_of_string "1"
	else
		let puiss = expo_mod x (div_big_int k (big_int_of_string "2") ) n in 
    if eq_big_int ( mod_big_int k (big_int_of_string "2") ) (big_int_of_string "0")
			then (mod_big_int (square_big_int puiss) n)
			else mod_big_int ( mult_big_int (mod_big_int (square_big_int puiss) n) x ) n;;


(*  Partie dechiffrement du message *)


let compose_rgb rgb= 
    let t = Array.make_matrix hauteur largeur (Graphics.rgb 255 0 0) in 
        for i = 0 to hauteur - 1 do
            for j = 0 to largeur - 1 do 
               t.(i).(j) <- Graphics.rgb rgb.(0).(i).(j) rgb.(1).(i).(j) rgb.(2).(i).(j)
            done
        done;
        t;;




(* DECHIFFRER*)

let img = decompose_rgb;;
let img_chiffre_r = img.(0);;

let array_to_list = Array.to_list (Array.concat (Array.to_list img_chiffre_r));;


let rec list_to_dechiffrem l = match l with
    | [] -> []
    | hd::tl -> (Char.escaped (String.get (bin_of_int hd) ((String.length (bin_of_int hd)) - 1 ) ) ) :: (list_to_dechiffrem tl);;

let liste_bit = list_to_dechiffrem array_to_list;;

let rec list_8_par_8 l= match l with
    | [] -> []
    | hd::x::x2::x3::x4::x5::x6::x7::tl -> ( hd ^ x ^ x2 ^ x3 ^ x4 ^ x5 ^ x6 ^ x7 )::(list_8_par_8 tl)
    | hd::tl ->  hd::list_8_par_8 tl;;

(* binaire vers entier vers string avec 2 digits *)
let digit_to_2digit d = 
    if (int_of_string ("0b" ^ d) ) < 10 then 
        "0" ^ (string_of_int (int_of_string ("0b" ^ d) ) )
    else
        (string_of_int (int_of_string ("0b" ^ d) ) );;

(* Pour dernier nombre du msg chiffre on met seulement 1 digit si compris entre 0 et 9*)
let dernier_digit d = 
    if (int_of_string ("0b" ^ d) ) < 10 then 
        (string_of_int (int_of_string ("0b" ^ d) ) )
    else
        (string_of_int (int_of_string ("0b" ^ d) ) );;

(* list string_binaire vers list int vers list string *)
let rec bin_to_s_int l= match l with
    | [] -> []
    | hd::tl ->  ( digit_to_2digit hd )::bin_to_s_int tl;;




(* https://ocaml.org/learn/tutorials/99problems.html *)
let slice list i k =
    let rec take n = function
      | [] -> []
      | h :: t -> if n = 0 then [] else h :: take (n-1) t
    in
    let rec drop n = function
      | [] -> []
      | h :: t as l -> if n = 0 then l else drop (n-1) t
    in
    take (k - i + 1) (drop i list);;

(* decoupage liste correspondant à la taille du msg chiffree donc *)
(* Concat de bin_to_s_int avec dernier_digit *)
(*Pour dernier nombre du msg chiffre on met seulement 1 digit si compris entre 0 et 9*)

let list_slice = if (msg_chiff_taille mod 2) = 1 then
                    list_to_string  ( (bin_to_s_int (slice (list_8_par_8 (liste_bit)) 0 (msg_chiff_taille/2 - 1))) @[(dernier_digit(List.nth (list_8_par_8 (liste_bit)) (msg_chiff_taille/2)))] )
                 else
                    list_to_string  ( (bin_to_s_int (slice (list_8_par_8 (liste_bit)) 0 ((msg_chiff_taille - 1)/2 - 1))) @[(dernier_digit(List.nth (list_8_par_8 (liste_bit)) ((msg_chiff_taille - 1)/2 )))] );;



let mdechiffre = expo_mod (big_int_of_string list_slice) d n;;
string_of_big_int mdechiffre;;
print_string ( "Message dechiffre :" ^ (string_of_big_int mdechiffre) ^ "\n" );; 
(* on assemble 2 à 2 les nombres et on les transforme en char puis on converti la liste de char obtenue vers une liste de string *)
let mdebase = list_to_string (List.map Char.escaped (list_2_par_2 (string_to_list(string_of_big_int mdechiffre))));;


print_string ("\n Message dans l'image : \n" ^ mdebase ^ "\n");;

